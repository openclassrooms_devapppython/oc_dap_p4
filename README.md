# ChessLogi

## Explications:

Chesslogi est un logiciel écrit en python pour générer des appareillements et
gérer des scores de tournois d'échecs.

## Requirements :

Le logiciel a besoin d'utiliser des librairies tierces pour fonctionner.  
Pour les installer, il faut exécuter la commande suivante:

```shell
pip install -r requirements.txt
```

## Lancer le programme:

Pour lancer le programme, il faut exécuter la commande suivante:

```shell
python main.py
```

Vous pourrez alors choisir parmi 3 options:

- Créer un tournoi
- Charger un tournoi
- Accéder aux rapports concernant différents tournois

## Aide

Vous serez guidés tout au long du programme par des messages qui seront
affichés dans la console pour vous indiquer où vous vous trouvez et quels sont
vos choix possibles.
