from models.player import Player

from views.verifiers.tournament_verifiers import tournament_date_verifier, \
    tournament_string_verifier


def add_player():
    last_name = tournament_string_verifier("nom du joueur")
    first_name = tournament_string_verifier("prénom du joueur")
    birthdate = tournament_date_verifier("date de naissance du joueur")
    player = Player(last_name=last_name, first_name=first_name,
                    birthdate=birthdate)
    return player
