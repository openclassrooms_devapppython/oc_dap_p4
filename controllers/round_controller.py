import random

from models.player import Player
from models.match import Match
from random import shuffle
from typing import List


def player_ranking(players: List[Player]) -> List[Player]:
    return sorted(players, key=lambda x: x.points, reverse=True)


def pairing(players: List[Player], round_number: int) -> List[Match]:
    list_of_matchs = []
    if round_number == 1:
        shuffle(players)
    else:
        players = player_ranking(players)

    if len(players) % 2 != 0:
        players.insert(random.randint(0, len(players) - 1),
                       Player(last_name="N/A", first_name="N/A",
                              birthdate="N/A"))

    players_copy = players.copy()
    while players_copy:
        match_found = False
        player1 = players_copy.pop(0)
        i = 0
        while i < len(players_copy) and not match_found:
            player2 = players_copy[i]
            if (player1 != player2 and player1
                    not in player2.already_faced_opponents):
                player2 = players_copy.pop(i)
                match_found = True
                shuffling_players = [player1, player2]
                shuffle(shuffling_players)
                list_of_matchs.append(
                    Match(shuffling_players[0], shuffling_players[1], 0, 0))
            i += 1
        if not match_found:
            player2 = players_copy.pop(0)
            match_found = True
            shuffling_players = [player1, player2]
            shuffle(shuffling_players)
            list_of_matchs.append(
                Match(shuffling_players[0], shuffling_players[1], 0, 0))
    return list_of_matchs
