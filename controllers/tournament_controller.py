from datetime import datetime

from models.tournament import Tournament
from models.round import Round
from views.display_points import display_points
from views.player_creation import player_creation
from views.prompt_match_winner import prompt_match_winner
from views.various_output import display_match, display_round_number

from views.verifiers.tournament_verifiers import (
    tournament_date_verifier,
    tournament_round_number_verifier,
    tournament_string_verifier, tournament_description_prompt
)

from controllers.round_controller import pairing


def start_tournament():
    tournament_name = tournament_string_verifier('nom du tournoi')
    tournament_location = tournament_string_verifier('lieu du tournoi')
    tournament_start_date = tournament_date_verifier('date du tournoi')
    tournament_rounds_number = tournament_round_number_verifier()
    tournament_description = tournament_description_prompt()

    players = player_creation()

    tournament = Tournament(name=tournament_name, location=tournament_location,
                            start_date=tournament_start_date,
                            rounds_number=tournament_rounds_number,
                            players=players,
                            description=tournament_description)

    return tournament


def play_round(tournament: Tournament):
    display_round_number(tournament.current_round + 1)
    start_date_hour = datetime.now().strftime("%Y-%m-%d %H:%M")
    round_in_progress = Round(
        matchs_list=pairing(tournament.players, tournament.current_round + 1),
        name="Round " + str(tournament.current_round + 1),
        start_date_hour=start_date_hour,
        end_date_hour='')
    for match in round_in_progress.matchs_list:
        display_match(match)
        winner = prompt_match_winner(match)
        match.winner(winner)
        if winner == '1':
            tournament.players[
                tournament.players.index(match.match[0][0])].points += 1
        elif winner == '2':
            tournament.players[
                tournament.players.index(match.match[1][0])].points += 1
        else:
            print("Saisie invalide")
            exit(1)

        tournament.players[tournament.players.index(match.match[0][0])].already_faced_opponents.append(
            match.match[1][0])
        tournament.players[tournament.players.index(match.match[1][0])].already_faced_opponents.append(
            match.match[0][0])
    display_points(tournament.players)
    round_in_progress.end_date = datetime.now().strftime("%d/%m/%Y %H:%M")
    tournament.rounds.append(round_in_progress)
    tournament.current_round += 1
