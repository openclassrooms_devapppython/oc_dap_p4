from views.game_in_progress import game_in_progress
from views.handle_main_menu_choices import game_start

if __name__ == "__main__":
    tournament = game_start()
    game_in_progress(tournament)
