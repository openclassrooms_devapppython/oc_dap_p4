from models.player import Player as P, Player, player_serializer


class Match:
    def __init__(self, player1: P, player2: P, score_player1, score_player2):
        self.match = ([player1, score_player1], [player2, score_player2])

    def __str__(self):
        return f"{self.match}"

    def __repr__(self):
        return f"{self.match}"

    def winner(self, winner):
        list_of_list = list(self.match)
        if winner == "1":
            list_of_list[0][1] += 1
        else:
            list_of_list[1][1] += 1
        self.match = tuple(list_of_list)


def match_serializer(matchs: list[Match]) -> list[str]:
    list_of_matchs = []
    for match in matchs:
        list_of_matchs.append(
            f"{player_serializer(match.match[0][0])} _ {match.match[0][1]} - "
            f"{match.match[1][1]} _ {player_serializer(match.match[1][0])}"
        )
    return list_of_matchs


def match_deserializer(matchs: list[str], player_list: list[Player]) -> list[Match]:
    list_of_matchs = []
    for match in matchs:
        teams = match.split("-")
        team1_score = teams[0].split("_")
        names = team1_score[0].split(", ")
        player1 = find_player_by_name(names[0], names[1], player_list)
        score1 = int(team1_score[1])

        team2_score = teams[1].split("_")
        names = team2_score[1].split(", ")
        player2 = find_player_by_name(names[0], names[1], player_list)
        score2 = int(team2_score[0])

        list_of_matchs.append(Match(player1, player2, score1, score2))

    return list_of_matchs


def find_player_by_name(player_first_name: str, player_last_name: str,
                        player_list: list[Player]) -> Player:
    for player in player_list:
        if (player.first_name.strip() == player_first_name.strip()
                and player.last_name.strip() == player_last_name.strip()):
            return player
    print("Joueur introuvable")
