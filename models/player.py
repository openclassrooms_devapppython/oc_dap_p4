class Player:
    def __init__(self, last_name, first_name, birthdate):
        self.last_name = last_name
        self.first_name = first_name
        self.birthdate = birthdate
        self.already_faced_opponents = []
        self.points = 0

    def __str__(self):
        return (f"Joueur : {self.last_name} {self.first_name}, né(e) le {self.birthdate}")

    def __repr__(self):
        return f"Joueur : {self.last_name} {self.first_name} {self.points}"


def player_serializer(player: Player) -> str:
    return f"{player.first_name}, {player.last_name}"


def player_serializer_with_birthdate(player: Player) -> str:
    return f"{player.first_name}, {player.last_name}, {player.birthdate}"


def multiple_player_serializer(players: list[Player]) -> list[str]:
    return [player_serializer_with_birthdate(player) for player in players]


def player_deserializer(json_data: list) -> list[Player]:
    player_list = []
    for player in json_data:
        player_info = player.split(",")
        player_list.append(
            Player(first_name=player_info[0], last_name=player_info[1],
                   birthdate=player_info[2]))
    return player_list
