from typing import Dict

from models.match import Match, match_serializer, match_deserializer
from models.player import Player


class Round:
    def __init__(self, matchs_list, name, start_date_hour, end_date_hour):
        self.name = name
        self.matchs_list: list[Match] = matchs_list
        self.start_date = start_date_hour
        self.end_date = end_date_hour

    def __str__(self):
        return f"""
        Nom du round: {self.name}
        Date de debut: {self.start_date}
        Date de fin: {self.end_date}
        Matchs: {str(self.matchs_list)}
        """


def round_serializer(rounds: list[Round]) -> list[Dict]:
    list_of_rounds = []
    for round in rounds:
        list_of_rounds.append({
            'name': round.name,
            'start_date': round.start_date,
            'end_date': round.end_date,
            'matchs': match_serializer(round.matchs_list)
        })
    return list_of_rounds


def round_deserializer(rounds: list[Dict], players_list: list[Player]) -> list[Round]:
    list_of_rounds = []
    for round in rounds:
        round_name = round['name']
        round_start_date = round['start_date']
        round_end_date = round['end_date']
        round_matchs = match_deserializer(round['matchs'], players_list)

        list_of_rounds.append(Round(name=round_name, start_date_hour=round_start_date, end_date_hour=round_end_date,
                                    matchs_list=round_matchs))

    return list_of_rounds
