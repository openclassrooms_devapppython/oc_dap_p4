from typing import Dict

from models.player import Player, multiple_player_serializer, \
    player_deserializer
from models.round import round_serializer, round_deserializer


class Tournament:
    def __init__(self, name, location, start_date, rounds_number, players,
                 description, ):
        self.name = name
        self.location = location
        self.start_date = start_date
        self.rounds_number = int(rounds_number)
        self.current_round = 0
        self.rounds = []
        self.players: list[Player] = players
        self.description = description

    def __str__(self):
        return f"""
        Nom du tournoi: {self.name}
        Lieu: {self.location}
        Description: {self.description}
        Date de début du tournoi: {self.start_date}
        Nombre de rounds: {self.rounds_number}
        Joueurs: {str(self.players)}
        """


def tournament_serializer(tournament: Tournament) -> Dict:
    return {
        'name': tournament.name,
        'location': tournament.location,
        'start_date': tournament.start_date,
        'rounds_number': tournament.rounds_number,
        'description': tournament.description,
        'rounds': round_serializer(tournament.rounds),
        'players': multiple_player_serializer(tournament.players)
    }


def tournament_deserializer(json_data: dict) -> Tournament:
    players = player_deserializer(json_data['players'])

    tournament_name = json_data['name']
    tournament_location = json_data['location']
    tournament_start_date = json_data['start_date']
    tournament_rounds_number = json_data['rounds_number']
    tournament_description = json_data['description']

    tournament = Tournament(name=tournament_name, location=tournament_location,
                            start_date=tournament_start_date,
                            rounds_number=tournament_rounds_number,
                            description=tournament_description,
                            players=players)

    tournament.current_round = len(json_data['rounds'])
    tournament.rounds = round_deserializer(json_data['rounds'], players)

    return tournament
