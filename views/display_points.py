from models.player import Player


def display_points(players: list[Player]) -> None:
    print(f"Points à la fin du tour {players}\n")
