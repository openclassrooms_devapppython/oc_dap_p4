import json
import os

from controllers.tournament_controller import play_round
from models.tournament import Tournament, tournament_serializer
from views.prompt_save import prompt_save


def game_in_progress(tournament: Tournament):
    print("Tournoi en cours")
    while tournament.current_round < tournament.rounds_number:
        play_round(tournament)
        prompt_save(tournament)
    file_path = f"{tournament.name}_{tournament.start_date}.json"
    if not os.path.exists('data'):
        os.mkdir('data')
    with open('data' + "/" + file_path, "w") as file:
        json.dump(tournament_serializer(tournament), file, default=str)
