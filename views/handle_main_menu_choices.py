from time import sleep

from controllers.tournament_controller import start_tournament
from views.reports.menu import report_menu
from views.load_tournament import load_tournament
from views.prompt_main_menu import prompt_main_menu


def game_start():
    prompt_main_menu()
    condition = True
    while condition:
        a = input("Que voulez-vous faire ? ").strip()
        match a:
            case "1":
                condition = False
                tournament = start_tournament()
                return tournament
            case "2":
                print("Chargement du tournoi")
                load_tournament()
            case "3":
                report_menu()
            case "4":
                print("\nFin du programme\n\n")
                exit()
            case _:
                print("Saisie invalide, veuillez recommencer")
                sleep(1)
