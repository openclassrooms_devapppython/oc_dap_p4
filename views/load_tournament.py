import json
import os

from controllers.tournament_controller import play_round
from models.tournament import tournament_deserializer, tournament_serializer

from views.prompt_save import prompt_save
from views.reports.utils import prompt_tournament_name


def load_tournament():
    tournament_name = prompt_tournament_name()
    print(f"Chargement du tournoi {tournament_name}")
    with open(f'data/{tournament_name}', 'r') as f:
        json_data = json.load(f)
        tournament = tournament_deserializer(json_data)
        print("Tournoi en cours")
        while tournament.current_round < tournament.rounds_number:
            play_round(tournament)
            prompt_save(tournament)
        file_path = f"{tournament.name}_{tournament.start_date}.json"
        if not os.path.exists('data'):
            os.mkdir('data')
        with open('data' + "/" + file_path, "w") as file:
            json.dump(tournament_serializer(tournament), file, default=str)
        exit(0)
