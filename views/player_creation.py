from time import sleep

from controllers.player_controller import add_player


def player_creation():
    stop = None
    players = []
    creation = input(
        "Voulez-vous créer un nouveau joueur ?\n1. Oui\n2. Non\n").strip()
    while stop is None:
        if creation == '1':
            players.append(add_player())
            creation = (input(
                "Voulez-vous ajouter un autre joueur ?\n1. Oui\n2. Non\n")
                        .strip())
        elif creation == '2':
            stop = True
        else:
            print("Saisie invalide, veuillez recommencer")
            sleep(1)
            creation = (input(
                "Voulez-vous ajouter un autre joueur ?\n1. Oui\n2. Non\n")
                        .strip())
    return players
