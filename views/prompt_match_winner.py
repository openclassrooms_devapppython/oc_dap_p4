from models.match import Match


def prompt_match_winner(match: Match):
    choice = input(
        f"Veuillez entrer le vainqueur : \n"
        f"1. pour {match.match[0][0].first_name + ' ' + match.match[0][0].last_name} \n"
        f"2. pour {match.match[1][0].first_name + ' ' + match.match[1][0].last_name} \n\n""").strip()
    return choice
