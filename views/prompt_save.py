import json
import os

from models.tournament import tournament_serializer


def prompt_save(tournament):
    print("Voulez-vous sauvegarder le tournoi ?")
    print("1. Oui")
    print("2. Non")
    a = input("Que voulez-vous faire ? ").strip()
    if a == "1":
        file_path = f"{tournament.name}_{tournament.start_date}.json"
        if not os.path.exists('data'):
            os.mkdir('data')
        with open('data' + "/" + file_path, "w") as file:
            json.dump(tournament_serializer(tournament), file, default=str)
        exit(0)
