from views.reports.report import alphabetic_player_list, tournament_list, \
    name_and_date_of_a_tournament, \
    players_of_a_tournament, rounds_and_matchs_of_a_tournament


def report_menu():
    print("\nBienvenue dans l'onglet de rapports")
    print("Entrez 1 pour lister tous les tournois par ordre alphabetique\n"
          "Entrez 2 pour lister tous les tournois\n"
          "Entrez 3 pour accéder au nom et aux dates d'un tournoi donné\n"
          "Entrez 4 lister tous les joueurs d'un tournoi par ordre "
          "alphabétique \n"
          "Entrez 5 pour lister tous les tours d'un tournoi et tous les matchs"
          " du tour \n"
          "Entrez autre chose pour revenir au menu principal \n"
          )
    condition = True
    while condition:
        a = input("\nQue voulez-vous faire ? ").strip()
        match a:
            case "1":
                alphabetic_player_list()
            case "2":
                tournament_list()
            case "3":
                name_and_date_of_a_tournament()
            case "4":
                players_of_a_tournament()
            case "5":
                rounds_and_matchs_of_a_tournament()
            case _:
                condition = False
    print("\n\nEntrez 1 pour lancer un nouveau tournoi\n"
          "Entrez 2 pour charger un tournoi \n"
          "Entrez 3 pour consulter un rapport \n"
          "Entrez 4 pour quitter le programme \n\n")
    return
