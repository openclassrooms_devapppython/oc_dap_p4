import json

from views.reports.utils import retrieve_tournaments, prompt_tournament_name


def alphabetic_player_list() -> None:
    tournaments_name_list = retrieve_tournaments()
    players_list = []
    for tournament_name in tournaments_name_list:
        tournament = json.load(open(f"data/{tournament_name}"))
        for player in tournament['players']:
            if player not in players_list:
                players_list.append(player)
    players_list.sort()
    for player in players_list:
        print(player)


def tournament_list() -> None:
    tournaments_name_list = retrieve_tournaments()
    for tournament_name in tournaments_name_list:
        print(tournament_name)


def name_and_date_of_a_tournament() -> None:
    tournament_name = prompt_tournament_name()
    tournament = json.load(open(f"data/{tournament_name}"))
    print(
        f"Nom du tournoi : {tournament['name']}, date de début : "
        f"{tournament['start_date']}")


def players_of_a_tournament() -> None:
    tournament_name = prompt_tournament_name()
    tournament = json.load(open(f"data/{tournament_name}"))
    players_list = []
    for player in tournament['players']:
        players_list.append(player)
    players_list.sort()
    for player in players_list:
        print(player)


def rounds_and_matchs_of_a_tournament() -> None:
    tournament_name = prompt_tournament_name()
    tournament = json.load(open(f"data/{tournament_name}"))
    round_count = len(tournament['rounds'])
    for i in range(round_count):
        print(f"Round {i + 1}")
        for match in tournament['rounds'][i]['matchs']:
            print(match)
