import os


def retrieve_tournaments():
    folder_path = 'data'
    tournament_in_folder = [f for f in os.listdir(folder_path) if
                            os.path.isfile(os.path.join(folder_path, f))]
    return tournament_in_folder


def prompt_tournament_name() -> str:
    tournaments_name_list = retrieve_tournaments()
    for i in range(len(tournaments_name_list)):
        print(i + 1, tournaments_name_list[i])
    print("\n")
    while True:
        a = int(input("Quel tournoi souhaitez-vous consulter ? ")) - 1
        if a >= len(tournaments_name_list) or a < 0:
            print("Saisie invalide, veuillez recommencer")
        else:
            return tournaments_name_list[a]
