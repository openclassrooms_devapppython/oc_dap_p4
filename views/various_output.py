from models.match import Match


def display_round_number(round_number: int) -> None:
    print(f"Ronde en cours : {round_number}")


def display_match(match: Match) -> None:
    print(match)
