from datetime import datetime


def tournament_string_verifier(field_name):
    while True:
        prompt = input(f"Entrez le {field_name}: ").strip()
        if 64 < len(prompt) < 1:
            print(
                f"Le {field_name} doit être compris entre 1 et 64 caractères,"
                f" veuillez recommencer")
            continue
        else:
            return prompt


def tournament_date_verifier(field_name):
    while True:
        prompt = input(f"Entrez la {field_name} (DD-MM-YYYY): ").strip()
        try:
            prompt = datetime.strptime(prompt, '%d-%m-%Y')
            return prompt
        except ValueError:
            print(f"La {field_name} n'est pas valide, veuillez recommencer")
            continue


def tournament_round_number_verifier():
    while True:
        prompt = input(
            "Entrez le nombre de rondes du tournoi: (4 par défaut) ").strip()
        try:
            prompt = int(prompt)
            if prompt < 1:
                print(
                    "Le nombre de rondes du tournoi ne peut pas être négatif, "
                    "veuillez recommencer")
                continue
            return prompt
        except ValueError:
            return 4


def tournament_description_prompt():
    return input("Entrez la description du tournoi: ").strip()
